package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def static peselNumbers = ['37071845517','47851752306','abd384hf92j','1q1w2e3r4t5','172836471🥓🥓','192837495827']
    def "should check correct size"() {
        given:
            UserId userId = new UserId(peselNumbers.get(0))
            UserId userId1 = new UserId(peselNumbers.get(1))
            UserId userId2 = new UserId(peselNumbers.get(2))
            UserId userId3 = new UserId(peselNumbers.get(3))
            UserId userId4 = new UserId(peselNumbers.get(4))
            UserId userId5 = new UserId(peselNumbers.get(5))
        when:
            def answer = [userId.isCorrectSize(),
                          userId1.isCorrectSize(),
                          userId2.isCorrectSize(),
                          userId3.isCorrectSize(),
                          userId4.isCorrectSize(),
                          userId5.isCorrectSize()]
        then:
            answer == [true,true,false,false,false,false]
    }

    def "should check sex"(){
        given:
        UserId userId = new UserId(peselNumbers.get(0))
        UserId userId1 = new UserId(peselNumbers.get(1))
        UserId userId2 = new UserId(peselNumbers.get(2))
        UserId userId3 = new UserId(peselNumbers.get(3))
        UserId userId4 = new UserId(peselNumbers.get(4))
        UserId userId5 = new UserId(peselNumbers.get(5))
        when:
        def answer = [userId.getSex(),
                      userId1.getSex(),
                      userId2.getSex(),
                      userId3.getSex(),
                      userId4.getSex(),
                      userId5.getSex()]
        then:
        answer == [Optional.of(UserId.getSexMen()),Optional.of(UserId.getSexWoman()),Optional.empty(),Optional.empty(),Optional.empty(),Optional.empty()]
    }

    def "should check if pesel is correct"(){
        given:
        UserId userId = new UserId(peselNumbers.get(0))
        UserId userId1 = new UserId(peselNumbers.get(1))
        UserId userId2 = new UserId(peselNumbers.get(2))
        UserId userId3 = new UserId(peselNumbers.get(3))
        UserId userId4 = new UserId(peselNumbers.get(4))
        UserId userId5 = new UserId(peselNumbers.get(5))
        when:
        def answer = [userId.isCorrect(),
                      userId1.isCorrect(),
                      userId2.isCorrect(),
                      userId3.isCorrect(),
                      userId4.isCorrect(),
                      userId5.isCorrect()]

        then:
        answer == [true,true,false,false,false,false]
    }

    def "should return date"(){
        given:
        UserId userId = new UserId(peselNumbers.get(0))
        UserId userId1 = new UserId(peselNumbers.get(1))
        UserId userId2 = new UserId(peselNumbers.get(2))
        UserId userId3 = new UserId(peselNumbers.get(3))
        UserId userId4 = new UserId(peselNumbers.get(4))
        UserId userId5 = new UserId(peselNumbers.get(5))
        when:
        def answer = [userId.getDate(),
                      userId1.getDate(),
                      userId2.getDate(),
                      userId3.getDate(),
                      userId4.getDate(),
                      userId5.getDate()]

        then:
        answer == [Optional.of('18-07-1937'),Optional.of('17-05-1847'),Optional.empty(),Optional.empty(),Optional.empty(),Optional.empty()]
    }
}
