package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if(isPeselValid())
        {
            Sex sex = Integer.parseInt(String.valueOf(id.charAt(9)))%2==0 ? Sex.WOMAN : Sex.MAN;
            return Optional.of(sex);
        }
        return Optional.empty();
    }

    @Override
    public boolean isCorrect() {
        return isPeselValid();
    }

    @Override
    public Optional<String> getDate() {
        if(isPeselValid()){
            return Optional.of(getBirthDay() + '-' + getBirthMonth() + '-' + getBirthYear());
        }
        return Optional.empty();
    }

    private boolean isNumber(){
        return id.matches("^[0-9]*$");
    }

    private boolean isPeselValid(){
        if(isCorrectSize()&&isNumber()){
            int[] controlNumbers = {9,7,3,1,9,7,3,1,9,7};
            int suma=0;
            for (int i = 0; i < id.length()-1; i++) {
                suma+= Integer.parseInt(String.valueOf(id.charAt(i)))*controlNumbers[i];
            }
            return suma % 10 == Integer.parseInt(String.valueOf(id.charAt(10)));
        }
        return false;
    }
    private String getBirthYear() {
        int month = Integer.parseInt(id.substring(2,4));
        int year = Integer.parseInt(id.substring(0,2));
        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }
        return String.valueOf(year);
    }

    private String getBirthMonth() {
        int month = Integer.parseInt(id.substring(2,4));
        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }
        if(month<10){
            return '0'+String.valueOf(month);
        }
        return String.valueOf(month);
    }

    private String getBirthDay() {
        int day = Integer.parseInt(id.substring(4,6));
        if(day<10){
            return '0'+String.valueOf(day);
        }
        return id.substring(4,6);
    }

    public static Sex getSexWoman(){
        return Sex.WOMAN;
    }

    public static Sex getSexMen(){
        return Sex.MAN;
    }

}
